# Fresco Logic FL2000 protocol documentation #

The knowledge summarized here are from researching packet dumps, poking
the device with random data, and daydreaming. You should assume
everything below is wrong until proven otherwise. It is also awfully
incomplete.

### Terms ###

The term *control packet* refer to a single usb frame transferred over USB control transfer.
The term *bulk packet* refer to a single usb frame transferred over USB bulk transfer.
The term *frame* refer to a frame viewed on the monitor (frames per second).

### Initialization ###

We start by sending a lot of control packets.
(TODO figure out each of them and document.)

After this is done, we need to send some bulk packets.
(TODO figure it out and document)

At this stage, the screen should be turned on (getting vga signal) but
still black.

### Transfer of frames ###

We can now start sending the actual images/frames to show on the screen.
Each frame consists of one or multiple bulk packets. The first packets
are up to 20480 bytes big, with the remainer of the frame in the second
last packet, and the last packet is empty (size 0).
Example:

* packet 0: 20544
* packet 1: 20544
* packet 3: 14656
* packet 4: 0


A 800x600 screen with only white pixels is only 64 bytes big. That
transfer looks like this:

* packet 0: 64
* packet 1: 0

From here on I will refer to a series of packets like this as a
*frame transfer*. We need to repeat sending these frame transfers, as
the device seems to have no memory.

I think that the packets have no special beginning or end. If this is
true we can just split the frame at appropriate packet size without
inserting any kind of extra characters when sending data.


### Format of frame transfers ###

The device has support for some kind of compression. It seems that a
picture with horizontal stripes are easy to compress and show. When
showing an image with horizontal stripes the device would hang until I
moved something more easily compressed to the display.

#### Poor man's frame transfer!! ####

We can send frames by completely ignoring compression. Each pixel
consists of two bytes. The first byte, byte A, can be anything.
The second byte, byte B, needs to be over 0x80 (at least 0x79 would
create a strobe light of my display).

Some colors:

* 0x00FF is yellow
* 0xFFFF is white
* 0x80FF is black
* 0xFF80 is blue


So a 800x600 frame buffer whould consist of 
800*600*2 = 960000 bytes (ABABABAB...ABABABAB).

Sending 0xFF, 0xFF, 0x80, 0xFF, 0xFF, 0xFF, 0x80, 0xFF... would create a
checkerboard pattern.


### End ###

To shut off the monitor (no vga signal) we send the following control
packets:

data 0x4d,0x08,0x01,0xd4

wIndex: 32828

and

data 0x4d,0x80,0xbc,0x0b

wIndex: 32840
