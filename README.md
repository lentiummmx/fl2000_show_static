# Fresco Logic FL2000 static image viewer #
The FL2000 is a ~5€ usb to vga adapter. It currently only has windows drivers. This project's goal is to be able to input an arbitrary image and output it on a display, using the FL2000. When this is working it should be possible to make a Linux driver.

### What devices is this for? ###

This is not for "DisplayLink" adapters which uses another protocol. They supposedly contain their own memory, so after a frame is pushed to the screen the CPU does not need to do any work. The FL2000 seems to go black when frames are no longer sent to the device.

These devices are supported by the windows driver.
```
USB\VID_1D5C&PID_2000   ; FL2000.
USB\VID_17EF&PID_7209   ; Lenovo.
USB\VID_1D5C&PID_1FFE   ; LENOFO
```

### What works? ###
I can currently write pixels to the screen, in multiple colors. I don't know how the compression works though, so the frames are bigger than needed and might not work well on USB 2.0. I can also "replay" packets to my monitor and they show up fine. I have no idea what they do though, so it might be that timings etc needs to be changed for other monitors. It is only 800x600 at the moment.

### What's left to do? ###
* Figure out what all "control packets" does.
* * Set resolution
* * ~~Turn on monitor~~  It works, I'm just not sure which one it is... :D
* * ~~Turn off monitor~~ Done
* Figure out protocol
* * ~~Show full screen in all possible colors would be a good start~~ (Done?)
* * Figure out how to convert RGB bytes to "FL2000 bytes"
* * Figure out "compression algorithm"

### How to run? ###
I have not been able to brick my device or my monitor, but I cannot be sure that it's safe for **your** equipment.

**By using this code you agree to take full responsibility for the consequenses, I take none. **

Compile with 
```
gcc fl2000_show_static.c -o fl2000_show_static -I/usr/include/libusb-1.0 -lusb-1.0 -Wall
```
Then run with 
```
fl2000_show_static 0
```

Try 0,1,2,3... for different modes

### Other sources ###
This project is based on the work of [cy384](http://www.cy384.com/projects/fl2000dx-driver.html) [[github fl2000_get_edid](https://github.com/cy384/fl2000_get_edid)]. Thank you!
